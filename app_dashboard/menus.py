MENUS = {
    'NAV_MENU_LEFT_ADMINISTRADOR': [
        {
            "name": "Registro Staff",
            'name_col': 'registro',
            "url": "/staff",
            "validators": ["menu_generator.validators.is_superuser"],
            "icon_class": 'fas fa-fw fa-cog',
            "submenu": [
                {
                    "name": "Crear",
                    "url": 'app_dashboard:registro-staff',
                },
                {
                    "name": "Listado",
                    "url": 'app_dashboard:listado-staff',
                },
            ],
        },
        {
            "name": "Producto",
            'name_col': 'producto',
            "url": "/producto",
            "validators": ["menu_generator.validators.is_staff"],
            "icon_class": 'fas fa-fw fa-cog',
            "submenu": [
                {
                    "name": "Crear",
                    "url": 'app_dashboard:registro-producto',
                },
                {
                    "name": "Listar",
                    "url": 'app_dashboard:listado-producto',
                },
            ],
        },
        {
            "name": "Producto en almacen",
            'name_col': 'productoalmacen',
            "url": "/producto-almacen",
            "validators": ["menu_generator.validators.is_staff"],
            "icon_class": 'fas fa-fw fa-cog',
            "submenu": [
                {
                    "name": "Crear",
                    "url": 'app_dashboard:registro-producto-almacen',
                },
                {
                    "name": "Listar",
                    "url": 'app_dashboard:listado-almacen-producto'
                },
            ],
        },
        {
            "name": "Movimiento Producto",
            'name_col': 'movimientiproducto',
            "validators": ["menu_generator.validators.is_staff"],
            "url": "/movimiento-producto",
            "icon_class": 'fas fa-fw fa-cog',
            "submenu": [
                {
                    "name": "Crear",
                    "url": 'app_dashboard:registro-movimiento-producto',
                },
                {
                    "name": "Listar",
                    "url": 'app_dashboard:listado-movimiento-producto',
                },
            ],
        }
    ]
}
