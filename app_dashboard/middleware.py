from abc import ABC, abstractclassmethod

from django.http.response import HttpResponseRedirect
from django.urls.base import resolve, reverse


class BaseMiddleware(ABC):
    def __init__(self, get_response, ls_urls_no_validadas, app_name, url_redireccion):
        self.get_response = get_response
        self.ls_urls_no_validadas = ls_urls_no_validadas
        self.app_name = app_name
        self.url_redireccion = url_redireccion

    def __call__(self, request):
        url = request.path
        url_resolved = resolve(url)

        if 'api' in url_resolved.namespaces:
            return self.get_response(request)

        if self.app_name == url_resolved.app_name and not url_resolved.url_name in self.ls_urls_no_validadas:
            if self.has_permission(request):
                return self.get_response(request)
            else:
                return HttpResponseRedirect(self.url_redireccion)

        else:
            return self.get_response(request)

    @abstractclassmethod
    def has_permission(self, request):
        pass


class DashboardMiddleware(BaseMiddleware):
    def __init__(self, get_response):
        super(DashboardMiddleware, self).__init__(
            get_response=get_response,
            ls_urls_no_validadas=['login'],
            app_name='app_dashboard',
            url_redireccion=reverse('app_dashboard:login')
        )

    def has_permission(self, request):
        if request.user.is_anonymous:
            return False
        return request.user.is_active and request.user.is_staff
