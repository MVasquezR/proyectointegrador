from django.contrib import admin
from django import forms

from app_dashboard.models.almacen import Almacen, AlamacenProducto
from app_dashboard.models.categoria import Categoria
from app_dashboard.models.cliente import Cliente
from app_dashboard.models.movimiento import Movimiento, MovimientoProducto
from app_dashboard.models.producto import Producto
from app_dashboard.models.staff import Staff
from app_dashboard.models.tipo_documento import TipoDocumento
from app_dashboard.models.tipo_unidad import Conversion, TipoUnidad
from app_dashboard.models.usuario import Usuario


class UsuarioForm(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = '__all__'


class UsuarioAdmin(admin.ModelAdmin):
    form = UsuarioForm
    list_display = ['first_name', 'last_name', 'email']


class AlmacenForm(forms.ModelForm):
    class Meta:
        model = Almacen
        fields = '__all__'


class AlmacenAdmin(admin.ModelAdmin):
    form = UsuarioForm
    list_display = ['nombre', 'descripcion', 'direccion']


class AlmacenProductoForm(forms.ModelForm):
    class Meta:
        model = AlamacenProducto
        fields = '__all__'


class AlmacenProductoAdmin(admin.ModelAdmin):
    form = AlmacenProductoForm
    list_display = ['almacen', 'producto', 'stock_actual']


class MovimientoProductoForm(forms.ModelForm):
    class Meta:
        model = MovimientoProducto
        fields = '__all__'


class MovimientoProductoAdmin(admin.ModelAdmin):
    form = MovimientoProductoForm
    list_display = ['movimiento', 'producto', 'cantidad']


admin.site.register(Usuario, UsuarioAdmin)
admin.site.register(Staff)
admin.site.register(Cliente)
admin.site.register(Categoria)
admin.site.register(Almacen, AlmacenAdmin)
admin.site.register(AlamacenProducto, AlmacenProductoAdmin)
admin.site.register(TipoDocumento)
admin.site.register(Producto)
admin.site.register(Movimiento)
admin.site.register(MovimientoProducto, MovimientoProductoAdmin)
admin.site.register(Conversion)
admin.site.register(TipoUnidad)
