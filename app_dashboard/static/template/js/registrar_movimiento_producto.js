let tabla_producto_almacen = $('#tabla_movimiento');
let array_almacen = [];

$(document).ready(function () {
    let dt_pro_alm = tabla_producto_almacen.DataTable();

    $('#agregar').on('click', function () {
        let producto_id = $('#productod').val();
        let producto_nombre = $('#productod').find(':selected').data('nombre');

        let tipo_movimiento_id = $('#tipo_movimientod').val()
        let tipo_movimiento_nombre = $('#tipo_movimientod').find(':selected').data('nombre');

        let fecha_movimiento = $('#fecha_movimiento').val();
        let cantidad = $('#cantidad').val();

        if (array_almacen.length > 0) {
            for (let i = 0; i < array_almacen.length; i++) {
                if (array_almacen[i]['producto_id'] === producto_id && array_almacen[i]['tipo_movimiento_id'] === tipo_movimiento_id) {
                    array_almacen[i]['cantidad'] = parseInt(array_almacen[i]['cantidad']) + parseInt(cantidad);
                } else {
                    array_almacen.push({
                        'fecha_movimiento': fecha_movimiento,
                        'cantidad': cantidad,
                        'producto_id': producto_id,
                        'tipo_movimiento_id': tipo_movimiento_id,
                    });
                }
            }
        } else {
            array_almacen.push({
                'fecha_movimiento': fecha_movimiento,
                'cantidad': cantidad,
                'producto_id': producto_id,
                'tipo_movimiento_id': tipo_movimiento_id,
            });
        }

        dt_pro_alm.row.add([fecha_movimiento, tipo_movimiento_nombre, producto_nombre, cantidad]).draw();
    });

    $('#guardar').on('click', function () {
        $.ajax({
            url: tabla_producto_almacen.data('url-save'),
            type: "POST",
            data: JSON.stringify(array_almacen),
            processData: false,
            contentType: "application/json",
            headers: {
                'X-CSRFToken': CSRF_TOKEN,
            },
            success: function (result) {
                window.location.href = tabla_producto_almacen.data('url-refresh')
            },
            error: function (result) {
                console.log(result)
            },
        });
    })
});