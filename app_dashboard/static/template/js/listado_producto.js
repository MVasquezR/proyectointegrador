let listado_producto = $('#listado_producto');
let url_eliminar = listado_producto.data('eliminar');
let url_crear_qr = listado_producto.data('crear-qr');

$(document).ready(function () {
    $.ajax({
        url: listado_producto.data('url-list'),
        type: "GET",
        success: function (data) {
            listado_producto.DataTable({
                data: data,
                columns: [
                    {"data": "nombre"},
                    {
                        "data": "categoria", "orderable": false, "searchable": false, "render":
                            function (data, type, row) {
                                return row.categoria.nombre;
                            }
                    },
                    {
                        "data": "tipo_unidad", "orderable": false, "searchable": false, "render":
                            function (data, type, row) {
                                return row.tipo_unidad.nombre;
                            }
                    },
                    {
                        "data": "acciones", "orderable": false, "searchable": false, "render":
                            function (data, type, row) {
                                return `<a href="${listado_producto.data('producto-edit')}${row.id}" target="_blank"><i class="fa fa-edit"></i></a> 
                                        <a href="#" class="eliminar" data-toggle="modal" data-target="#modalEliminar" target="" data-id="${row.id}"><i class="fas fa-trash-alt"></i></a> 
                                        <a href="#" class="crear_qr" data-id="${row.id}" target="" data-toggle="modal" data-target="#modalCrearQR"> <i class="fas fa-align-justify"></i></a>`;
                            }
                    }
                ]
            })
        },
        error: function (result) {
            console.log(result)
        },
    });

    listado_producto.on('click', '.eliminar', function () {
        $('.modal_eliminar').attr('data-id', $(this).data('id'));
    });

    $('#modalEliminar').on('click', '.modal_eliminar', function () {
        let url = url_eliminar.slice(0, -2);
        $.ajax({
            url: url + $(this).data('id'),
            type: "DELETE",
            headers: {
                'X-CSRFToken': CSRF_TOKEN,
            },
            success: function (result) {
                window.location.href = listado_producto.data('url-refresh')
            },
            error: function (result) {
                console.log(result)
            },
        })
    });

    listado_producto.on('click', '.crear_qr', function () {
        $('.modal_QR').attr('data-id', $(this).data('id'));
    });

    $('#modalCrearQR').on('click', '.modal_QR', function () {
        $.ajax({
            url: url_crear_qr,
            data: {'id': $(this).data('id')},
            type: "POST",
            headers: {
                'X-CSRFToken': CSRF_TOKEN,
            },
            success: function (result) {
                window.location.href = listado_producto.data('url-refresh')
            },
            error: function (result) {
                console.log(result)
            },
        })
    });
});

