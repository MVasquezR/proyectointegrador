let form = $('#form');

$(document).ready(function () {
    form.on('submit', function (e) {
        e.preventDefault();
        let formData = new FormData(this);

        $.ajax({
            url: form.data('url-edit'),
            type: "PUT",
            data: formData,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRFToken': CSRF_TOKEN,
            },
            success: function (result) {
                // console.log(result)
                window.location.href = form.data('url-refresh')
            },
            error: function (result) {
                console.log(result)
            },
        });
    });

    $('#imprimir').on('click', function () {
        let  url = $(this).data('id');
        let w = window.open(url);
        w.window.print();
    })
});