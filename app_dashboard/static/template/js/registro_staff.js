let form = $('#form');

$(document).ready(function () {

    form.on('submit', function (e) {
        e.preventDefault();
        let formData = new FormData(this);

        $.ajax({
            url: form.data('url-save'),
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRFToken': CSRF_TOKEN,
            },
            success: function (result) {
                window.location.href = form.data('url-refresh')
            },
            error: function (result) {
                let error = result.responseJSON;
                let html = '';
                $.each(error, function (index, contenido) {
                    html += '<div class="alert alert-danger alert-dismissible" role="alert" id="alerta_div">' +
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                        '<span aria-hidden="true">&times;</span>' +
                        '</button>' +
                        '<strong id="variable">' + index + '</strong> ' + contenido[0] +
                        '</div>';
                });
                $('#contenido_alertas').html(html)
            },
        });
    });
});