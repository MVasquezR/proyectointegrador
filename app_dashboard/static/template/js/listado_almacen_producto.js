let listado_almacen_producto = $('#listado_almacen_producto')

$(document).ready(function () {
    $.ajax({
        url: listado_almacen_producto.data('url-list'),
        type: "GET",
        success: function (data) {
            listado_almacen_producto.DataTable({
                data: data,
                columns: [
                    {
                        "data": "producto", "orderable": false, "searchable": false, "render":
                            function (data, type, row) {
                                return data.nombre;
                            }
                    },
                    {
                        "data": "almacen", "orderable": false, "searchable": false, "render":
                            function (data, type, row) {
                                return data.nombre;
                            }
                    },
                    {"data": "stock_actual"},
                    {
                        "data": "acciones", "orderable": false, "searchable": false, "render":
                            function (data, type, row) {
                                return `<a id="div_delete" href="#" data-id="${row.id}" data-toggle="modal" data-target="#modalEliminar" target=""><i class="fas fa-trash-alt"></i></a>`;
                            }
                    }
                ]
            })
        },
        error: function (result) {
            console.log(result)
        },
    });

    $('#listado_almacen_producto').on('click', '#div_delete', function () {
        $('.modal_eliminar').attr('data-id', $(this).data('id'));
    });

    $('#modalEliminar').on('click', '.modal_eliminar', function () {
        let url_cv = listado_almacen_producto.data('delete').substring(0, listado_almacen_producto.data('delete').length - 2);
        let id = $(this).data('id');
        $.ajax({
            url: url_cv + id,
            type: "DELETE",
            headers: {
                'X-CSRFToken': CSRF_TOKEN,
            },
            success: function (result) {
                console.log(result)
                window.location.href = listado_almacen_producto.data('url-refresh')
            },
            error: function (result) {
                console.log(result)
            },
        });
    });
});

