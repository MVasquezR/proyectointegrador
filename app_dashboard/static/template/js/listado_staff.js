let listado_staff = $('#listado_staff');
let url_eliminar = listado_staff.data('eliminar');

$(document).ready(function () {
    $.ajax({
        url: listado_staff.data('url-list'),
        type: "GET",
        success: function (data) {
            listado_staff.DataTable({
                data: data,
                columns: [
                    {"data": "nombre"},
                    {"data": "apellido_paterno"},
                    {"data": "apellido_materno"},
                    {"data": "edad"},
                    {"data": "correo"},
                    {"data": "numero_de_documento"},
                    {"data": "telefono_o_celular"},
                    {
                        "data": "acciones", "orderable": false, "searchable": false, "render":
                            function (data, type, row) {
                                return `<a href="${listado_staff.data('staff-edit')}${row.id}" ><i class="fa fa-edit"></i></a>
                                        <a href="#" class="eliminar" data-id="${row.id}" data-toggle="modal" data-target="#modalEliminar" target=""><i class="fas fa-trash-alt"></i></a>`;
                            }
                    }
                ]
            })
        },
        error: function (result) {
            console.log(result)
        },
    });

    listado_staff.on('click', '.eliminar', function () {
        $('.modal_eliminar').attr('data-id', $(this).data('id'));
    });

    $('#modalEliminar').on('click', '.modal_eliminar', function () {
        let url = url_eliminar.slice(0, -2);
        $.ajax({
            url: url + $(this).data('id'),
            type: "DELETE",
            headers: {
                'X-CSRFToken': CSRF_TOKEN,
            },
            success: function (result) {
                window.location.href = listado_staff.data('url-refresh')
            },
            error: function (result) {
                console.log(result)
            },
        })
    });

});
