let tabla_producto_almacen = $('#tabla_producto_almacen');
let array_almacen = [];

$(document).ready(function () {
    let dt_pro_alm = tabla_producto_almacen.DataTable();

    $('#agregar').on('click', function () {
        let producto_id = $('#productod').val();
        let producto_nombre = $('#productod').find(':selected').data('nombre');

        let almacen_id = $('#almacen').val();
        let almacen_nombre = $('#almacen').find(':selected').data('nombre');

        let stock_minimo = $('#stock_minimo').val();
        let stock_maximo = $('#stock_maximo').val();
        let stock_actual = $('#stock_actual').val();

        if (array_almacen.length > 0) {
            for (let i = 0; i < array_almacen.length; i++) {
                if (array_almacen[i]['almacen'] === almacen_id && array_almacen[i]['producto'] === producto_id) {
                    array_almacen[i]['stock_minimo'] = parseInt(array_almacen[i]['stock_minimo']) + parseInt(stock_minimo);
                    array_almacen[i]['stock_maximo'] = parseInt(array_almacen[i]['stock_maximo']) + parseInt(stock_maximo);
                    array_almacen[i]['stock_actual'] = parseInt(array_almacen[i]['stock_actual']) + parseInt(stock_actual);
                } else {
                    array_almacen.push({
                        'almacen': almacen_id,
                        'producto': producto_id,
                        'stock_minimo': stock_minimo,
                        'stock_maximo': stock_maximo,
                        'stock_actual': stock_actual
                    });
                }
            }
        } else {
            array_almacen.push({
                'almacen': almacen_id,
                'producto': producto_id,
                'stock_minimo': stock_minimo,
                'stock_maximo': stock_maximo,
                'stock_actual': stock_actual
            });
        }
        dt_pro_alm.row.add([almacen_nombre, producto_nombre, stock_minimo, stock_maximo, stock_actual]).draw();
    });

    $('#guardar').on('click', function () {
        $.ajax({
            url: tabla_producto_almacen.data('url-save'),
            type: "POST",
            data: JSON.stringify(array_almacen),
            processData: false,
            contentType: "application/json",
            headers: {
                'X-CSRFToken': CSRF_TOKEN,
            },
            success: function (result) {
                window.location.href = tabla_producto_almacen.data('url-refresh');
            },
            error: function (result) {
                console.log(result)
            },
        });
    })
});