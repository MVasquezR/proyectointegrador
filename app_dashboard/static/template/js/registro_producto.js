let form = $('#form');

$(document).ready(function () {
    form.on('submit', function (e) {
        e.preventDefault();
        let formData = new FormData(this);

        $.ajax({
            url: form.data('url-save'),
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            headers: {
                'X-CSRFToken': CSRF_TOKEN,
            },
            success: function (result) {
                window.location.href = form.data('url-refresh')
            },
            error: function (result) {
                console.log(result)
            },
        });
    });
});