from django.db import models

from app_dashboard.utilitarios.genericos import CleanCharField


class Categoria(models.Model):
    nombre = CleanCharField(blank=False, null=False, max_length=100)
    descripcion = CleanCharField(blank=False, null=False, max_length=200)

    def __str__(self):
        return self.nombre
