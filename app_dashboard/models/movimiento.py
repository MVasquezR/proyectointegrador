from django.db import models
from datetime import datetime

from app_dashboard.models.producto import Producto
from app_dashboard.models.usuario import Usuario

TIPO_MOVIMIENTO = (
    ('entrada', 'entrada'),
    ('salida', 'salida')
)


class Movimiento(models.Model):
    fecha_movimiento = models.DateTimeField(null=False, blank=False)
    tipo_movimiento = models.CharField(null=False, blank=False, choices=TIPO_MOVIMIENTO, max_length=50)
    usuario = models.ForeignKey(Usuario, on_delete=False)
    movimiento_producto = models.ManyToManyField(Producto, through="MovimientoProducto")

    def __str__(self):
        return self.tipo_movimiento + ' - ' + self.usuario.first_name


class MovimientoProducto(models.Model):
    movimiento = models.ForeignKey(Movimiento, on_delete=models.CASCADE)
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE)
    cantidad = models.IntegerField(null=False, blank=False)
