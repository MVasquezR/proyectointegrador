from django.db import models

from app_dashboard.models.producto import Producto
from app_dashboard.utilitarios.genericos import CleanCharField


class Almacen(models.Model):
    nombre = CleanCharField(blank=False, null=False, max_length=100)
    descripcion = CleanCharField(blank=False, null=False, max_length=200)
    direccion = CleanCharField(blank=False, null=False, max_length=200)
    almacen_producto = models.ManyToManyField(Producto, through='AlamacenProducto')

    def __str__(self):
        return self.nombre


class AlamacenProducto(models.Model):
    almacen = models.ForeignKey(Almacen, on_delete=models.CASCADE)
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE)
    stock_minimo = models.IntegerField()
    stock_maximo = models.IntegerField()
    stock_actual = models.IntegerField()

    def __str__(self):
        return self.almacen.nombre + ' - ' + self.producto.nombre
