import os

from django.db import models

from app_dashboard.models.categoria import Categoria
from app_dashboard.models.tipo_unidad import TipoUnidad
from app_dashboard.utilitarios.genericos import CleanCharField


class Producto(models.Model):
    def image_path(instance, filename):
        filename, file_extension = os.path.splitext(filename)
        return os.path.join('codigo_qr', 'producto-' + instance.nombre + file_extension)

    nombre = CleanCharField(blank=False, null=False, max_length=100)
    descripcion = CleanCharField(blank=False, null=False, max_length=200)
    categoria = models.ForeignKey(Categoria, on_delete=False)
    tipo_unidad = models.ForeignKey(TipoUnidad, on_delete=False)
    precio = models.FloatField(null=False, blank=False)
    codigo_qr = CleanCharField(blank=True, null=True, max_length=100)

    def __str__(self):
        return self.nombre
