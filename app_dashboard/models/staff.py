from app_dashboard.models.usuario_abstract import UsuarioAbstract
from django.utils.datetime_safe import datetime


class Staff(UsuarioAbstract):
    pass

    @property
    def edad(self):
        ahora = datetime.now()
        if ahora.month > self.fecha_de_nacimiento.month:
            return ahora.year - self.fecha_de_nacimiento.year

        elif ahora.month == self.fecha_de_nacimiento.month:
            if ahora.day >= self.fecha_de_nacimiento.day:
                return ahora.year - self.fecha_de_nacimiento.year
            else:
                return ahora.year - self.fecha_de_nacimiento.year - 1

        else:
            return ahora.year - self.fecha_de_nacimiento.year - 1
