from django.conf.urls import url
from django.urls import include

from app_dashboard.views import *

urlpatterns = [
    url(r'^api/', include(('app_dashboard.api.urls', 'api'), namespace='api')),
    url(r'^login/', LoginViewSet.as_view(), name='login'),
    url(r'^logout/', LogoutView.as_view(), name='salida'),
    url(r'^registro-staff/', RegistroStaffViewSet.as_view(), name='registro-staff'),
    url(r'^registro-producto-almacen/', RegistroAlmacenEnProductoViewSet.as_view(), name='registro-producto-almacen'),
    url(r'^registro-movimiento-producto/', RegistroMovimientoProductoViewSet.as_view(), name='registro-movimiento-producto'),
    url(r'^registro-producto/', RegistroProductoViewSet.as_view(), name='registro-producto'),
    url(r'^editar-staff/$', EditarStaffViewSet.as_view(), name='editar-staff'),
    url(r'^editar-producto/$', EditarProductoViewSet.as_view(), name='editar-producto'),
    url(r'^editar-staff/(?P<pk>\d+)/$', EditarStaffViewSet.as_view(), name='editar-staff'),
    url(r'^editar-producto/(?P<pk>\d+)/$', EditarProductoViewSet.as_view(), name='editar-producto'),
    url(r'^listado-staff/', ListadoStaffViewSet.as_view(), name='listado-staff'),
    url(r'^listado-producto/', ListadoProductoViewSet.as_view(), name='listado-producto'),
    url(r'^listado-almacen-producto/', ListadoAlmacenProductoViewSet.as_view(), name='listado-almacen-producto'),
    url(r'^listado-movimiento-producto/', ListadoMovimientoProductoViewSet.as_view(), name='listado-movimiento-producto'),
    url(r'^erro-pagina/', ErrorPaginaViewSet.as_view(), name='erro-pagina'),
]
