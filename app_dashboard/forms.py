from django.contrib.auth import authenticate
from django.contrib.auth.forms import AuthenticationForm
from django import forms
from django.core.exceptions import ValidationError


class LoginForm(AuthenticationForm):
    username = forms.CharField(widget=forms.TextInput(
        attrs={"type": "email", "name": "username", "id": "username", "class": "form-control form-control-user"}),
        required=True)
    password = forms.CharField(widget=forms.TextInput(
        attrs={"type": "password", "name": "password", "id": "password", "class": "form-control form-control-user"}),
        required=True)

    def confirm_login_allowed(self, user):
        if not (user.is_staff and user.is_active and not user.is_block):
            raise forms.ValidationError(
                self.error_messages['inactive'],
                code='inactive',
            )

    def clean(self):
        clean_data = self.cleaned_data
        self.user_cache = authenticate(**{'username': clean_data['username'], 'password': clean_data['password']})
        if self.user_cache is None:
            raise ValidationError("Datos incorrectos.")
        else:
            self.confirm_login_allowed(self.user_cache)

        return self.cleaned_data
