from django.conf import settings
from django.contrib.auth import logout
from django.contrib.auth.views import LoginView
from django.http import HttpResponseRedirect
from django.shortcuts import render, resolve_url, redirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters

from app_dashboard.forms import LoginForm
from app_dashboard.models.almacen import Almacen
from app_dashboard.models.categoria import Categoria
from app_dashboard.models.movimiento import TIPO_MOVIMIENTO
from app_dashboard.models.producto import Producto
from app_dashboard.models.staff import Staff
from app_dashboard.models.tipo_documento import TipoDocumento
from app_dashboard.models.tipo_unidad import TipoUnidad
from app_dashboard.models.usuario_abstract import GENERO
from app_dashboard.utilitarios.genericos import get_dict_with_choices


class BaseView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.contexto = {}

    def insert_personal_context(self, **kwargs):
        '''
        Insertar contexto propio de la vista
        :return:
        '''
        pass

    def get_contexto(self, **kwargs):
        '''
        Retornar el contexto para retornar la vista
        :return: dict
        '''
        self.insert_personal_context(**kwargs)
        return self.contexto


class CustomLoginVIew(LoginView):

    def is_valid_user(self):
        pass

    @method_decorator(sensitive_post_parameters())
    @method_decorator(csrf_protect)
    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        if self.is_valid_user():
            redirect_to = self.get_success_url()
            if redirect_to == self.request.path:
                raise ValueError(
                    "Redirection loop for authenticated user detected. Check that "
                    "your LOGIN_REDIRECT_URL doesn't point to a login page."
                )
            return HttpResponseRedirect(redirect_to)
        return super(LoginView, self).dispatch(request, *args, **kwargs)


class LoginViewSet(CustomLoginVIew):
    form_class = LoginForm
    template_name = 'login.html'

    def is_valid_user(self):
        return hasattr(self.request.user, 'is_staff') and self.request.user.is_active and not self.request.user.is_block

    def get_success_url(self):
        url = self.get_redirect_url()
        if self.request.user.is_superuser:
            return url or resolve_url('app_dashboard:registro-staff')
        else:
            return url or resolve_url('app_dashboard:registro-producto')


class LogoutView(View):

    def get(self, request):
        logout(request)
        return redirect(reverse('app_dashboard:login'))


class RegistroStaffViewSet(BaseView):

    def insert_personal_context(self, **kwargs):
        self.contexto['ls_tipo_documento'] = TipoDocumento.objects.all()
        self.contexto['dict_genero'] = get_dict_with_choices(GENERO)

    def get(self, request):
        if request.user.is_superuser:
            return render(request, 'registro_staff.html', self.get_contexto())
        else:
            return render(request, 'error_404.html')


class ErrorPaginaViewSet(View):
    def get(self, request):
        return render(request, 'registro_staff.html')


class EditarStaffViewSet(BaseView):

    def insert_personal_context(self, **kwargs):
        self.contexto['staff'] = kwargs['staff']
        self.contexto['ls_tipo_documento'] = TipoDocumento.objects.all()
        self.contexto['dict_genero'] = get_dict_with_choices(GENERO)

    def get(self, request, pk=None):
        staff = Staff.objects.get(id=int(pk))
        return render(request, 'editar_staff.html', self.get_contexto(**{'staff': staff}))


class EditarProductoViewSet(BaseView):

    def insert_personal_context(self, **kwargs):
        self.contexto['producto'] = kwargs['producto']
        self.contexto['path_img'] = settings.MEDIA_URL + 'qr_product_' + kwargs['producto'].nombre + '.png'
        self.contexto['ls_categoria'] = Categoria.objects.all()
        self.contexto['ls_tipo_unidad'] = TipoUnidad.objects.all()

    def get(self, request, pk=None):
        producto = Producto.objects.get(id=int(pk))
        return render(request, 'editar_producto.html', self.get_contexto(**{'producto': producto}))


class ListadoStaffViewSet(BaseView):

    def get(self, request):
        if request.user.is_superuser:
            return render(request, 'listado_staff.html')
        else:
            return render(request, 'error_404.html')


class ListadoProductoViewSet(BaseView):

    def get(self, request):
        return render(request, 'listado_producto.html')


class ListadoAlmacenProductoViewSet(BaseView):

    def get(self, request):
        return render(request, 'listado_almacen_producto.html')


class ListadoMovimientoProductoViewSet(BaseView):

    def get(self, request):
        return render(request, 'listado_movimiento_producto.html')


class RegistroAlmacenEnProductoViewSet(BaseView):
    def insert_personal_context(self, **kwargs):
        self.contexto['ls_producto'] = Producto.objects.all()
        self.contexto['ls_almacen'] = Almacen.objects.all()

    def get(self, request):
        return render(request, 'registro_producto_en_almacen.html', self.get_contexto())


class RegistroMovimientoProductoViewSet(BaseView):

    def insert_personal_context(self, **kwargs):
        self.contexto['ls_producto'] = Producto.objects.all()
        self.contexto['dict_movimiento'] = get_dict_with_choices(TIPO_MOVIMIENTO)

    def get(self, request):
        return render(request, 'registro_movimiento_de_producto.html', self.get_contexto())


class RegistroProductoViewSet(BaseView):

    def insert_personal_context(self, **kwargs):
        self.contexto['ls_categoria'] = Categoria.objects.all()
        self.contexto['ls_tipo_unidad'] = TipoUnidad.objects.all()

    def get(self, request):
        return render(request, 'registro_producto.html', self.get_contexto())
