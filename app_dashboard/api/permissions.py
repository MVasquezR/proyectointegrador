from rest_framework import permissions
from rest_framework.exceptions import PermissionDenied


class AdminPermission(permissions.BasePermission):
    '''
    Este permiso solo evalua si esta logueado con una cuenta de Admin
    '''

    def has_permission(self, request, view):
        user = request.user
        if hasattr(user, 'is_superuser') and user.is_superuser:
            return True
        raise PermissionDenied()


class StaffGeneralPermission(permissions.BasePermission):
    '''
    Este permiso solo evalua si esta logueado con una cuenta de Staff
    '''

    def has_permission(self, request, view):
        user = request.user
        if hasattr(user, 'is_staff') and user.is_staff:
            return True
        raise PermissionDenied()
