from rest_framework import routers

from app_dashboard.api.views import *

router = routers.DefaultRouter()
router.register(r'crear-staff', CrearStaffViewSet, base_name='crear-staff')
router.register(r'crear-producto', CrearProductoViewSet, base_name='crear-producto')
router.register(r'crear-alamacen-producto', CrearAlmacenProductoViewSet, base_name='crear-almacen-producto')
router.register(r'crear-movimiento-producto', CrearMovimientoProductoViewSet, base_name='crear-movimiento-producto')
router.register(r'crear-qr', CrearQRViewSet, base_name='crear-qr')
router.register(r'editar-staff', EditarStaffViewSet, base_name='editar-staff')
router.register(r'editar-producto', EditarProductoViewSet, base_name='editar-producto')
router.register(r'listar-staff', ListarStaffViewSet, base_name='listar-staff')
router.register(r'listar-producto', ListarProductoViewSet, base_name='listar-producto')
router.register(r'listar-almacen-producto', ListarAlmacenProductoViewSet, base_name='listar-almacen-producto')
router.register(r'listar-movimiento-producto', ListarMovimientoProductoViewSet, base_name='listar-movimiento-producto')
router.register(r'eliminar-almacen-producto', EliminarAlmacenProductoViewSet, base_name='eliminar-almacen-producto')
router.register(r'eliminar-movimiento-producto', EliminarMovimientoProductoViewSet, base_name='eliminar-movimiento-producto')
router.register(r'eliminar-producto', EliminarProductoViewSet, base_name='eliminar-producto')
router.register(r'eliminar-staff', EliminarStaffViewSet, base_name='eliminar-staff')

urlpatterns = router.urls
