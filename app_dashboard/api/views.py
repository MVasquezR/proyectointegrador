import os

import qrcode
from django.db import transaction
from rest_framework import mixins, status
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from ProjectIntegrador.settings import MEDIA_ROOT
from app_dashboard.api.permissions import AdminPermission, StaffGeneralPermission
from app_dashboard.api.serializers import *
from app_dashboard.models.almacen import Almacen
from app_dashboard.models.movimiento import MovimientoProducto, Movimiento
from app_dashboard.models.producto import Producto
from app_dashboard.models.staff import Staff


class CrearStaffViewSet(mixins.CreateModelMixin, GenericViewSet):
    permission_classes = (AdminPermission,)
    serializer_class = StaffSerializer
    queryset = Staff.objects.all()


class CrearProductoViewSet(mixins.CreateModelMixin, GenericViewSet):
    permission_classes = (AdminPermission, StaffGeneralPermission,)
    serializer_class = PorductoSerializer
    queryset = Producto.objects.all()

    def create(self, request, *args, **kwargs):
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=10,
            border=1,
        )
        data_categoria = Categoria.objects.get(id=int(request.POST['categoria']))
        data_tipo_unidad = TipoUnidad.objects.get(id=int(request.POST['tipo_unidad']))
        data = {
            'nombre': request.POST['nombre'],
            'descripcion': request.POST['descripcion'],
            'categoria': {
                'nombre': data_categoria.nombre,
                'descripcion': data_categoria.descripcion
            },
            'tipo_unidad': {
                'nombre': data_tipo_unidad.nombre,
                'descripcion': data_tipo_unidad.descripcion,
                'conversion': {
                    'nombre': data_tipo_unidad.conversion.nombre,
                    'descripcion': data_tipo_unidad.conversion.descripcion
                }
            },
            'precio': request.POST['precio']
        }
        qr.add_data(data)
        qr.make(fit=True)
        img = qr.make_image(fill_color="black", back_color="white")
        ruta_save = os.path.join(MEDIA_ROOT, 'qr_product_' + request.POST['nombre']) + '.png'
        img.save(ruta_save)
        post = request.POST.copy()
        post['codigo_qr'] = ruta_save
        serializer = self.get_serializer(data=post)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class CrearAlmacenProductoViewSet(mixins.CreateModelMixin, GenericViewSet):
    permission_classes = (AdminPermission, StaffGeneralPermission,)
    serializer_class = AlmacenProductoSerializer
    queryset = AlamacenProducto.objects.all()

    @transaction.atomic()
    def create(self, request, *args, **kwargs):
        for data in request.data:
            data_almacen = Almacen.objects.get(id=int(data['almacen']))
            data_producto = Producto.objects.get(id=int(data['producto']))
            almacen = AlamacenProducto(stock_actual=data['stock_actual'], stock_maximo=data['stock_maximo'],
                                       stock_minimo=data['stock_minimo'], almacen=data_almacen, producto=data_producto)
            almacen.save()
        return Response(request.data, status=status.HTTP_200_OK, template_name=None, content_type=None)


class CrearQRViewSet(mixins.CreateModelMixin, GenericViewSet):
    permission_classes = (AdminPermission, StaffGeneralPermission,)
    serializer_class = PorductoSerializer
    queryset = Producto.objects.all()

    @transaction.atomic()
    def create(self, request, *args, **kwargs):
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=10,
            border=1,
        )
        producto = Producto.objects.get(id=request.data['id'])
        data = {
            'nombre': producto.nombre,
            'descripcion': producto.descripcion,
            'categoria': {
                'nombre': producto.categoria.nombre,
                'descripcion': producto.categoria.descripcion
            },
            'tipo_unidad': {
                'nombre': producto.tipo_unidad.nombre,
                'descripcion': producto.tipo_unidad.descripcion,
                'conversion': {
                    'nombre': producto.tipo_unidad.conversion.nombre,
                    'descripcion': producto.tipo_unidad.conversion.descripcion
                }
            },
            'precio': producto.precio
        }
        qr.add_data(data)
        qr.make(fit=True)
        img = qr.make_image(fill_color="black", back_color="white")
        ruta_save = os.path.join('media', 'qr_product_' + producto.nombre) + '.png'
        img.save(ruta_save)
        producto.codigo_qr = ruta_save
        producto.save()
        return Response(request.data, status=status.HTTP_200_OK, template_name=None, content_type=None)


class CrearMovimientoProductoViewSet(mixins.CreateModelMixin, GenericViewSet):
    permission_classes = (AdminPermission, StaffGeneralPermission,)
    serializer_class = MovimientoProductoSerializer
    queryset = MovimientoProducto.objects.all()

    @transaction.atomic()
    def create(self, request, *args, **kwargs):
        for data in request.data:
            movimiento = Movimiento(fecha_movimiento=data['fecha_movimiento'], usuario=request.user,
                                    tipo_movimiento=data['tipo_movimiento_id'])
            movimiento.save()
            data_producto = Producto.objects.get(id=int(data['producto_id']))
            mov_prod = MovimientoProducto(movimiento=movimiento, producto=data_producto, cantidad=int(data['cantidad']))
            mov_prod.save()
        return Response(request.data, status=status.HTTP_200_OK, template_name=None, content_type=None)


class ListarStaffViewSet(mixins.ListModelMixin, GenericViewSet):
    permission_classes = (StaffGeneralPermission,)
    serializer_class = StaffListSerializer
    queryset = Staff.objects.all()


class ListarProductoViewSet(mixins.ListModelMixin, GenericViewSet):
    permission_classes = (AdminPermission, StaffGeneralPermission,)
    serializer_class = PorductoListSerializer
    queryset = Producto.objects.all()


class ListarAlmacenProductoViewSet(mixins.ListModelMixin, GenericViewSet):
    permission_classes = (AdminPermission, StaffGeneralPermission,)
    serializer_class = AlmacenProductoListarSerializer
    queryset = AlamacenProducto.objects.all()


class ListarMovimientoProductoViewSet(mixins.ListModelMixin, GenericViewSet):
    permission_classes = (AdminPermission, StaffGeneralPermission,)
    serializer_class = ListMovimientoProductoSerializer
    queryset = MovimientoProducto.objects.all()


class EditarStaffViewSet(mixins.UpdateModelMixin, GenericViewSet):
    permission_classes = (AdminPermission, StaffGeneralPermission,)
    serializer_class = StaffSerializer
    queryset = Staff.objects.all()


class EliminarProductoViewSet(mixins.DestroyModelMixin, GenericViewSet):
    permission_classes = (AdminPermission, StaffGeneralPermission,)
    serializer_class = PorductoSerializer
    queryset = Producto.objects.all()


class EliminarStaffViewSet(mixins.DestroyModelMixin, GenericViewSet):
    permission_classes = (AdminPermission,)
    serializer_class = StaffListSerializer
    queryset = Staff.objects.all()


class EditarProductoViewSet(mixins.UpdateModelMixin, GenericViewSet):
    permission_classes = (AdminPermission, StaffGeneralPermission,)
    serializer_class = PorductoSerializer
    queryset = Producto.objects.all()

    def update(self, request, *args, **kwargs):
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=10,
            border=1,
        )
        data_categoria = Categoria.objects.get(id=int(request.POST['categoria']))
        data_tipo_unidad = TipoUnidad.objects.get(id=int(request.POST['tipo_unidad']))
        data = {
            'nombre': request.POST['nombre'],
            'descripcion': request.POST['descripcion'],
            'categoria': {
                'nombre': data_categoria.nombre,
                'descripcion': data_categoria.descripcion
            },
            'tipo_unidad': {
                'nombre': data_tipo_unidad.nombre,
                'descripcion': data_tipo_unidad.descripcion,
                'conversion': {
                    'nombre': data_tipo_unidad.conversion.nombre,
                    'descripcion': data_tipo_unidad.conversion.descripcion
                }
            },
            'precio': request.POST['precio']
        }
        qr.add_data(data)
        qr.make(fit=True)
        img = qr.make_image(fill_color="black", back_color="white")
        ruta_save = os.path.join(MEDIA_ROOT, 'qr_product_' + request.POST['nombre']) + '.png'
        img.save(ruta_save)
        post = request.POST.copy()
        # post['codigo_qr'] = ruta_save
        instance = self.get_object()
        instance.nombre = post['nombre']
        instance.descripcion = post['descripcion']
        instance.categoria = data_categoria
        instance.tipo_unidad = data_tipo_unidad
        instance.precio = post['precio']
        instance.codigo_qr = ruta_save
        instance.save()

        serializer = self.get_serializer(instance)

        return Response(serializer.data)


class EliminarAlmacenProductoViewSet(mixins.DestroyModelMixin, GenericViewSet):
    permission_classes = (AdminPermission, StaffGeneralPermission,)
    serializer_class = AlmacenProductoSerializer
    queryset = AlamacenProducto.objects.all()


class EliminarMovimientoProductoViewSet(mixins.DestroyModelMixin, GenericViewSet):
    permission_classes = (AdminPermission, StaffGeneralPermission,)
    serializer_class = MovimientoProductoSerializer
    queryset = MovimientoProducto.objects.all()
