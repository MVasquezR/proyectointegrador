from django.conf import settings
from rest_framework import serializers

from app_dashboard.models.almacen import AlamacenProducto, Almacen
from app_dashboard.models.categoria import Categoria
from app_dashboard.models.movimiento import MovimientoProducto, Movimiento
from app_dashboard.models.producto import Producto
from app_dashboard.models.staff import Staff
from app_dashboard.models.tipo_unidad import TipoUnidad, Conversion


class StaffSerializer(serializers.ModelSerializer):
    class Meta:
        model = Staff
        fields = ['nombre', 'apellido_paterno', 'apellido_materno', 'fecha_de_nacimiento', 'tipo_documento',
                  'numero_de_documento', 'genero', 'telefono_o_celular', 'correo']

    fecha_de_nacimiento = serializers.DateField(input_formats=settings.DATE_INPUT_FORMATS, )


class StaffListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Staff
        fields = ['id', 'nombre', 'apellido_paterno', 'apellido_materno', 'edad', 'numero_de_documento',
                  'telefono_o_celular', 'correo']


class PorductoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Producto
        fields = '__all__'


class AlmacenProductoSerializer(serializers.ModelSerializer):
    class Meta:
        model = AlamacenProducto
        fields = '__all__'


class MovimientoProductoSerializer(serializers.ModelSerializer):
    class Meta:
        model = MovimientoProducto
        fields = '__all__'


class CategoriaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Categoria
        fields = '__all__'


class ConversionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Conversion
        fields = '__all__'


class TipoUnidadSerializer(serializers.ModelSerializer):
    conversion = ConversionSerializer()

    class Meta:
        model = TipoUnidad
        fields = ['nombre', 'descripcion', 'conversion']


class PorductoListSerializer(serializers.ModelSerializer):
    categoria = CategoriaSerializer()
    tipo_unidad = TipoUnidadSerializer()

    class Meta:
        model = Producto
        fields = ['id', 'nombre', 'categoria', 'tipo_unidad']


class AlmacenSerializer(serializers.ModelSerializer):
    class Meta:
        model = Almacen
        fields = '__all__'


class AlmacenProductoListarSerializer(serializers.ModelSerializer):
    almacen = AlmacenSerializer()
    producto = PorductoSerializer()

    class Meta:
        model = AlamacenProducto
        fields = ['id', 'almacen', 'producto', 'stock_actual']


class MovimientoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movimiento
        fields = '__all__'


class ListMovimientoProductoSerializer(serializers.ModelSerializer):
    movimiento = MovimientoSerializer()
    producto = PorductoSerializer()

    class Meta:
        model = MovimientoProducto
        fields = ['id', 'movimiento', 'producto', 'cantidad']
