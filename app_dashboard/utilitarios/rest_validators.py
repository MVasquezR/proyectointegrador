import os

from rest_framework.exceptions import ValidationError


def validate_file_extension(value, ls_of_extensions):
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    if not ext.lower() in ls_of_extensions:
        raise ValidationError('La extension del archivo no es soportada.')
